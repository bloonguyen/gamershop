<?php

return [
	'uploadPath' => $_SERVER['DOCUMENT_ROOT'] . '/imgs/uploads/products/',
	'uploadUrl' => '/imgs/uploads/products/',
	'bannerUploadPath' => $_SERVER['DOCUMENT_ROOT'] . '/imgs/uploads/banners/',
	'bannerUploadUrl' => '/imgs/uploads/banners/'
];
?>
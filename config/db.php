<?php

$config = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];

$config = array_merge(
    $config,
    require(__DIR__ . '/db-local.php')
);

return $config;

# Local development guide

This guides developers to set up a local development site for testing on their own local machine

## Requirements

These requirements must be installed

1. Vagrant
2. Virtualbox

## Step-by-step guide

1. Make sure you have an internet connection
2. Clone the project and its submodule

        git clone --recursive <url>

3. If you forgot to include `--recursive` in step 2, clone the submodules manually

        git submodule update --init

4. Go to the project's folder, where the file named `Vagrantfile` is located. Run vagrant. It will create a virtual machine and run provision on first start.

        vagrant up

5. If there are some errors, run provision once more (There is currently a bug with the provisioning script)

        vagrant up --provision

6. Open a browser, try connecting to your site via `http://localhost:8080/`
7. Add the three configuration files as mentioned in `Server Configurations` section. You may also set the value of `YII_DEBUG` in `index.php` to `true`, as this is a development environment. There should be a database already configured with username `vagrant` and password `vagrant`. Use `localhost` for database host name.

    To enable `gii`, your `main-local.php` should be edited to something like:

        <?php
        $config = [
            'components' => [
                'request' => [
                    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
                    'enableCookieValidation' => true,
                    'cookieValidationKey' => 'OFw-c6MFZhreoh-MZ5g2vQgOGFjgSqHd',
                ],
            ],
        ];

        if (YII_ENV_DEV) {
            // configuration adjustments for 'dev' environment
            $config['bootstrap'][] = 'debug';
            $config['modules']['debug'] = [
                'class' => 'yii\debug\Module',
            ];

            $config['bootstrap'][] = 'gii';
            $config['modules']['gii'] = [
                'class' => 'yii\gii\Module',
                'allowedIPs' => ["*"]
            ];
        }

        return $config;


8. SSH to the newly created virtual machine. Go to the mounted project's folder (Default to `/vagrant`)

        vagrant ssh
        cd /vagrant

9. Create these folders inside `/vagrant`: `logs`, `runtime`. Restart apache2

        sudo service apache2 restart

10. Install `composer`

        curl -sS https://getcomposer.org/installer | php
        sudo mv composer.phar /usr/local/bin/composer

11. Run `composer global require "fxp/composer-asset-plugin:*" && composer update` to download necessary requirements. The script may fail at some point due to missing access key from github. Follow the url mentioned in the error message, login to you github account, then generate the token, then paste the token into the terminal (the token will be invisible), then press enter to continue.

12. You may want to install `phpmyadmin` and `vim` for the sake of making your life easier. It's not a requirement, and you may never need it, though.
13. Run migrations for role-based access control and application's models.

        yii migrate --migrationPath=@yii/rbac/migrations
        yii migrate/up

14. Initialize user roles and add an admin user to login

        yii rbac/init
        yii rbac/create-user

# Basic vagrant knowledge

## Commands

1. Start the virtual machine: `vagrant up`
2. Suspend the virtual machine: `vagrant suspend`
3. Reload the virtual machine: `vagrant reload`
4. Force provisioning: `vagrant provision`
5. Documentation: `man vagrant`

## Configurations

The configurations is written in the file `Vagrantfile`. You can change which folder should this project's folder mount to in the virtual machine, or configure which ports should be forwarded to which of the vm's ports when accessing from the host machine.

## Provisioning

This project use chef for provisioning. The cookbooks are located in `/chef` folder. `/chef/chef-cookbooks` contains opensource cookbooks. It is an external submodule, so don't touch it unless a bug happens. `/chef/site-cookbooks` contains the recipe for this project's development environment.

# Server configuration

1. Upload the source code to a server, ignore these files and folders `chef/`, `.cache/`, `assets`, `runtime`, `.git/`, `.vagrant`, `.bundle`, `LICENSE`, `LICENSE.md`, `README.md`, `index-test.php`, `requirements.php`, `robots.txt`, `Vagrantfile`, `yii`, `yii.bat`, `.bowerrc`, `.gitignore`, `.gitmodules`. Edit `.htaccess` if necessary.
2. Create 2 new folders `assets` and `runtime` and `chmod` them to `775`. Do not chmod them to `777`
3. Copy `config/main-local.template.php` and rename it to `config/main-local.php`. Edit it appropriately
4. Copy `config/params-local.template.php` and rename it to `config/params-local.php`. Edit it appropriately
5. Copy `config/db-local.template.php` and rename it to `config/db-local.php`. Edit it appropriately
6. Open `index.php`, and change the value of the constant `YII_DEBUG` to `false`, if it's not already `false`.
7. Import a working database.

# Basic migrations commands

**[Official Documentation](http://www.yiiframework.com/doc-2.0/guide-db-migrations.html)**

1. Initialize role-based access control database:

        ./yii migrate --migrationPath=@yii/rbac/migrations/

2. Check and run missing migrations:

        ./yii migrate

3. Create a new migration:

        ./yii migrate/create custom_migration_name

    Note: The migration codes are located in `/migrations` folder. Remember to edit both the `up()` and `down()` functions

4. Revert a migration:

        ./yii migrate/down

5. View migration history:

        ./yii migrate/history all

# Role-based access control

**[Official Documentation](http://www.yiiframework.com/doc-2.0/guide-security-authorization.html#configuring-rbac)**

<?php

namespace app\controllers;

use \DateTime;
use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\Cookie;
use app\models\LoginForm;
use app\models\ExportForm;
use app\models\ProductOrder;
use yii\data\ActiveDataProvider;
use app\models\Category;
use app\models\Product;
use app\models\Banner;
use app\models\OrderItem;
use yii\web\NotFoundHttpException;

class HomeController extends Controller
{
    // public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : NULL,
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionCart() {
        return $this->render('cart_view');
    }

    public function actionConfirm($code) {

        return $this->render('order_confirm',[
                'code' => $code
        ]);
    }

    public function actionContact() {

        return $this->render('contact');
    }    

    public function actionOrder() {
        $model = new ProductOrder();
        $products = Yii::$app->cart->getPositions();


        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) {
                foreach ($products as $product) {
                    $order_item = new OrderItem();
                    $order_item->order_id = $model->id;
                    $order_item->product_id  =  $product->id;
                    $order_item->quantity  =  $product->getQuantity();
                    $order_item->price  =  $product->getCost();
                    $order_item->discounted_price  =  $product->getCost(true);
                    $order_item->save();
                }
                Yii::$app->cart->removeAll();
                return Yii::$app->response->redirect(Url::to(['/home/confirm/', 'code' => $model->code]));
            }
        }

        return $this->render('order_info',[
                'model' => $model
            ]);
    }

    public function actionItem($id) {
        if (($model = Product::findOne($id)) !== null) {
            return $this->render('product_detail', [
                    'model' => $model
                ]);
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCategory($id) {
        if (($category = Category::findOne($id)) !== null) {
            $dataProvider = new ActiveDataProvider([
                'query' => $category->getProducts(SORT_DESC),
                'pagination' => [
                    'pageSize' => 36
                ]
            ]);
            return $this->render('product_list', [
                    'category' => $category,
                    'dataProvider' => $dataProvider
                ]);
        }
        throw new NotFoundHttpException('The requested page does not exist.');

    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAddToCart($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            Yii::$app->cart->put($model, Yii::$app->request->post('quantity'));
            return true;
        }
        return false;
    }


    public function actionRemoveFromCart($id)
    {
        Yii::$app->cart->removeById($id);
        return true;
    }

    public function actionSearch()
    {
        $query = Yii::$app->request->get('query');
        $order = ProductOrder::findOne(['code' => $query]);
        if ($order) {
            return $this->render('view_order',[
                'order' => $order
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Product::find()
                            ->where(['like', 'name', $query])
                            ->orWhere(['like', 'description', $query]),
                'pagination' => [
                    'pageSize' => 36
                ]
            ]);
            return $this->render('product_list', [
                    'dataProvider' => $dataProvider
            ]);
        }
    }

    public function actionLanguage()
    {
        $language = Yii::$app->request->post('language');
        Yii::$app->language = $language;

        $languageCookie = new Cookie([
            'name' => 'language',
            'value' => $language,
            'expire' => time() + 60 * 60 * 24 * 30, // 30 days
        ]);
        Yii::$app->response->cookies->add($languageCookie);
        // return Yii::$app->language;
        return Yii::$app->response->redirect(Yii::$app->request->referrer);
    }

}

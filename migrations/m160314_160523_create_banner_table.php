<?php

use yii\db\Schema;
use yii\db\Migration;

class m160314_160523_create_banner_table extends Migration
{
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('banner', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'banner_image'=> Schema::TYPE_STRING . ' NOT NULL',
            'status' => Schema::TYPE_BOOLEAN. ' NOT NULL DEFAULT 1'       
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('banner');
        return true;
    }
}

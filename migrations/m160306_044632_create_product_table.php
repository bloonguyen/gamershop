<?php

use yii\db\Schema;
use yii\db\Migration;

class m160306_044632_create_product_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('product', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'promotion_id' => Schema::TYPE_INTEGER . ' REFERENCES promotion(id)',
            'price' => Schema::TYPE_INTEGER . ' NOT NULL',
            'description' => Schema::TYPE_TEXT. ' NOT NULL',
            'status' => Schema::TYPE_BOOLEAN . " NOT NULL DEFAULT 1"
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('product');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

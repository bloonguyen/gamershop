<?php

use yii\db\Schema;
use yii\db\Migration;

class m160321_124620_add_code_to_order extends Migration
{
    public function safeUp()
    {
        $this -> addColumn("product_order", "code", Schema::TYPE_STRING . " NOT NULL" );
    }

    public function safeDown()
    {
        $this -> dropColumn("product_order", "code");
        return true;
    }
}


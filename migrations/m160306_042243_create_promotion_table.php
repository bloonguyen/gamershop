<?php

use yii\db\Schema;
use yii\db\Migration;

class m160306_042243_create_promotion_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('promotion', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'rate' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'type' => Schema::TYPE_BOOLEAN . " NOT NULL DEFAULT 0",
            'start_date' => Schema::TYPE_DATE . ' NOT NULL',
            'end_date' => Schema::TYPE_DATE . ' NOT NULL'
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('promotion');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

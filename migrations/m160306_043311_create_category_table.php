<?php

use yii\db\Schema;
use yii\db\Migration;

class m160306_043311_create_category_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('category', [
            'id' => Schema::TYPE_PK,
            'parent_id' => Schema::TYPE_INTEGER . ' REFERENCES category(id)',
            'promotion_id' => Schema::TYPE_INTEGER . ' REFERENCES promotion(id)',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'status' => Schema::TYPE_BOOLEAN . " NOT NULL DEFAULT 1"
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('category');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

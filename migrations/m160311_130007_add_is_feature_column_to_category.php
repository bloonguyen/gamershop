<?php

use yii\db\Schema;
use yii\db\Migration;

class m160311_130007_add_is_feature_column_to_category extends Migration
{

    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this -> addColumn("category", "is_feature", Schema::TYPE_BOOLEAN . " NOT NULL DEFAULT 0");
        $this -> addColumn("category", "num_items", Schema::TYPE_INTEGER . " NOT NULL DEFAULT 3");
    }

    public function safeDown()
    {
        $this -> dropColumn("category", "is_feature");
        $this -> dropColumn("category", "num_items");
        return true;
    }
    
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m160306_051020_create_order_item_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('order_item', [
            'id' => Schema::TYPE_PK,
            'order_id' => Schema::TYPE_INTEGER . ' REFERENCES product_order(id)',
            'product_id' => Schema::TYPE_INTEGER . ' REFERENCES product(id)',
            'quantity' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
            'price' => Schema::TYPE_INTEGER. ' NOT NULL',
            'discounted_price' => Schema::TYPE_INTEGER . " NOT NULL"
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('order_item');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

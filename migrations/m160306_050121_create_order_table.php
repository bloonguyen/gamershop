<?php

use yii\db\Schema;
use yii\db\Migration;

class m160306_050121_create_order_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('product_order', [
            'id' => Schema::TYPE_PK,
            'customer_name' => Schema::TYPE_STRING . ' NOT NULL',
            'address' => Schema::TYPE_STRING . ' NOT NULL',
            'phone' => Schema::TYPE_STRING . ' NOT NULL',
            'note' => Schema::TYPE_TEXT. ' DEFAULT \'\'',
            'payment_method' => Schema::TYPE_STRING . " NOT NULL DEFAULT 'cod'",
            'status' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT 1"
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('product_order');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m160310_094720_drop_promotion_id_from_category_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('category', 'promotion_id');
    }

    public function safeDown()
    {
        $this->addColumn('category', 'promotion_id', Schema::TYPE_INTEGER . ' REFERENCES promotion(id)');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

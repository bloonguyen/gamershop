<?php

use yii\db\Schema;
use yii\db\Migration;

class m160310_095057_add_category_id_to_product extends Migration
{
    public function safeUp()
    {
        $this -> addColumn("product", "category_id", Schema::TYPE_INTEGER . " REFERENCES category(id)");
    }

    public function safeDown()
    {
        $this -> dropColumn("product", "category_id");
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

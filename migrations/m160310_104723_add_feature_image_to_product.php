<?php

use yii\db\Schema;
use yii\db\Migration;

class m160310_104723_add_feature_image_to_product extends Migration
{
    public function safeUp()
    {
        $this -> addColumn("product", "feature_image_1", Schema::TYPE_STRING . ' NOT NULL');
        $this -> addColumn("product", "feature_image_2", Schema::TYPE_STRING . ' NOT NULL');
        $this -> addColumn("product", "feature_image_3", Schema::TYPE_STRING . ' NOT NULL');
        $this -> addColumn("product", "feature_image_4", Schema::TYPE_STRING . ' NOT NULL');
        $this -> addColumn("product", "feature_image_5", Schema::TYPE_STRING . ' NOT NULL');
    }

    public function safeDown()
    {
        $this -> dropColumn("product", "feature_image_1");
        $this -> dropColumn("product", "feature_image_2");
        $this -> dropColumn("product", "feature_image_3");
        $this -> dropColumn("product", "feature_image_4");
        $this -> dropColumn("product", "feature_image_5");
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

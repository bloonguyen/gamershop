<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use app\models\User;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAllPermissions();
        $auth->removeAllRules();
        $auth->removeAllRoles();

        // Create roles
        $administrator = $auth->createRole('administrator');
        $moderator = $auth->createRole('moderator');

        // Add roles
        $auth->add($administrator);
        $auth->add($moderator);

        // Set up permissions hierarchy
        $auth->addChild($administrator, $moderator); // Admin can do whatever an moderator can

        return 0;
    }

    public function actionCreateUser() {
        $this->stdout("Create a new user...\n", Console::BOLD);

        $username = $this->prompt("Username: ", ['validator' => function ($input, &$error) {
                if (preg_match('/^[\w\d_\.]+$/', $input)) {
                    return true;
                }
                $error = "Username can only contain alphabet characters, numbers, dots and underscores.";
                return false;
            }]);

        $name = trim($this->prompt("Display name: ", ['validator' => function ($input, &$error) {
            $input = trim($input);
            if (preg_match('/^[\w\d_\. ]+$/', $input)) {
                return true;
            }
            $error = "Display name must contain at least 1 visible character, and can only contain alphabet characters, numbers, dots, underscores and spaces. Leading and trailing spaces will be trimmed.";
            return false;
        }]));

        $password = $this->prompt("Password: ", ['validator' => function ($input, &$error) {
                if (preg_match('/^.{8,}$/', $input)) {
                    return true;
                }
                $error = "Password must have at least 8 characters";
                return false;
            }]);

        $email = $this->prompt("Email: ", ['validator' => function ($input, &$error) {
                if (filter_var($input, FILTER_VALIDATE_EMAIL)) {
                    return true;
                }

                $error = "Invalid email format";
                return false;
            }]);

        $role = $this->prompt("Role (administrator/moderator): ", ['validator' => function ($input, &$error) {
                if (preg_match('/^(administrator|moderator)$/', $input)) {
                    return true;
                }

                $error = "Role must be either 'administrator' or 'moderator'";
                return false;
            }]);

        if ($this->confirm("Do you want to proceed with creating this user?")) {
            $model = new User();
            $model->scenario = User::SCENARIO_CONSOLE_CREATE;
            $model->setAttributes([
                    'username' => $username,
                    'name' => $name,
                    'email' => $email,
                    'status' => User::STATUS_ACTIVE
                ]);

            $model->setPassword($password);
            $model->generateAuthKey();
            $model->generateAccessToken();

            if ($model->save()) {
                $auth = Yii::$app->authManager;
                $role = $auth->getRole($role);
                $auth->assign($role, $model->id);
                $this->stdout("User '$username' has been created\n", Console::BOLD);
            } else {
                foreach ($model->errors as $key => $errors) {
                    foreach ($errors as $error) {
                        $this->stdout("'$key': $error", Console::BOLD);
                    }
                }
            }

        } else {
            $this->stdout("Aborting...\n", Console::BOLD);
        }

        return 0;
    }

    public function actionAssign($username, $role_name) {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($role_name);

        $user = User::findByUsername($username);
        if ($user !== null) {
            if ($role !== null) {
                $auth->revokeAll($user->id);
                $auth->assign($role, $user->id);
                $this->stdout("Role '$role_name' has been assigned to user '$username'\n", Console::BOLD);
            } else {
                $this->stdout("Aborting... Role '$role_name' does not exist.\n", Console::BOLD);
            }
        } else {
            $this->stdout("Aborting... User '$username' does not exist.\n", Console::BOLD);
        }
    }
}
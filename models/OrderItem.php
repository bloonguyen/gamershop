<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_item".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $quantity
 * @property integer $price
 * @property integer $discounted_price
 */
class OrderItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'quantity', 'price', 'discounted_price'], 'integer'],
            [['price', 'discounted_price'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => Yii::t('order_item', 'Order ID'),
            'product_id' => Yii::t('order_item', 'Product ID'),
            'quantity' => Yii::t('order_item', 'Quantity'),
            'price' => Yii::t('order_item', 'Price'),
            'discounted_price' => Yii::t('order_item', 'Discounted Price'),
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promotion".
 *
 * @property integer $id
 * @property string $name
 * @property integer $rate
 * @property integer $type
 * @property string $start_date
 * @property string $end_date
 */
class Promotion extends \yii\db\ActiveRecord
{

    const ALL = 1;
    const CUSTOM = 0;

    public $product_ids;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promotion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'start_date', 'end_date'], 'required'],
            [['rate', 'type'], 'integer'],
            [['start_date', 'end_date', 'product_ids'], 'safe'],
            [['start_date', 'end_date'], 'default', 'value' => 0],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('promotion', 'Name'),
            'rate' => Yii::t('promotion', 'Rate'),
            'type' => Yii::t('promotion', 'Type'),
            'start_date' => Yii::t('promotion', 'Start Date'),
            'end_date' => Yii::t('promotion', 'End Date'),
        ];
    }
}

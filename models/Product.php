<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;
/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property integer $promotion_id
 * @property integer $price
 * @property string $description
 * @property integer $status
 */
class Product extends \yii\db\ActiveRecord implements CartPositionInterface
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    
    public $feature_image_file_1;
    public $feature_image_file_2;
    public $feature_image_file_3;
    public $feature_image_file_4;
    public $feature_image_file_5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'description', 'category_id'], 'required'],
            [['promotion_id', 'price', 'status'], 'integer'],
            [['description', 'feature_image_1', 'feature_image_2', 'feature_image_3', 'feature_image_4', 'feature_image_5'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['feature_image_file_1', 'feature_image_file_2', 'feature_image_file_3', 'feature_image_file_4', 'feature_image_file_5'], 'file', 'extensions'=>'jpg, jpeg, png']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('product', 'ID'),
            'category_id' => Yii::t('product', 'Category'),
            'name' => Yii::t('product', 'Name'),
            'promotion_id' => Yii::t('product', 'Promotion'),
            'price' => Yii::t('product', 'Price'),
            'description' => Yii::t('product', 'Description'),
            'status' => Yii::t('product', 'Status'),
            'feature_image_1' => Yii::t('product', 'Feature Image 1'),
            'feature_image_2' => Yii::t('product', 'Feature Image 2'),
            'feature_image_3' => Yii::t('product', 'Feature Image 3'),
            'feature_image_4' => Yii::t('product', 'Feature Image 4'),
            'feature_image_5' => Yii::t('product', 'Feature Image 5'),
        ];
    }

    public function getImageFile($attribute)
    {
        return isset($this->$attribute) && !empty($this->$attribute) ? Yii::$app->params['uploadPath'] . $this->$attribute : null;
    }

    public function getImageUrl($attribute)
    {
        return isset($this->$attribute) && !empty($this->$attribute) ? Yii::$app->params['uploadUrl'] . $this->$attribute : '/images/default.png';
    }

    public function uploadImage($attribute) {
        $image = UploadedFile::getInstance($this, $attribute);

        // if no image was uploaded abort the upload
        if (empty($image)) {
            return false;
        }

        $ext = end((explode(".", $image->name)));

        // generate a unique file name
        $imgAttributeMap = [
                'feature_image_file_1' => 'feature_image_1',
                'feature_image_file_2' => 'feature_image_2',
                'feature_image_file_3' => 'feature_image_3',
                'feature_image_file_4' => 'feature_image_4',
                'feature_image_file_5' => 'feature_image_5'
            ];
        $this->$imgAttributeMap[$attribute] = Yii::$app->security->generateRandomString().".{$ext}";

        // the uploaded image instance
        return $image;
    }

    public function deleteImage($attribute) {
        $file = $this->getImageFile($attribute);

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->$attribute = null;

        return true;
    }

    public function beforeDelete() {
        foreach(['feature_image_1', 'feature_image_2', 'feature_image_3', 'feature_image_4', 'feature_image_5'] as $attribute) {
            $this->deleteImage($attribute);
        }
        return true;
    }

    use CartPositionTrait;

    public function getPromotion(){
        return $this->hasOne(Promotion::classname(), ['id' => 'promotion_id'])->andWhere(['and', 'now() > start_date', 'now() < end_date']);
    }

    public function getPrice($withDiscount = false)
    {
        if($withDiscount) {
            $promotion = $this->promotion;
            if($promotion) {
                return $this->price * (1 - ($promotion->rate / 100));
            }
        }
        return $this->price;
    }

    public function getCost($withDiscount = false)
    {
        return $this->getPrice($withDiscount) * $this->getQuantity();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCategory($order = SORT_ASC) {
        return Category::find()
            ->where(['id' => $this->category_id])
            ->orderBy(['id' => $order])->one();
    }

}

<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $banner_image
 */
class Banner extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    
    public $banner_image_file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'], 
            [['status'], 'integer'],
            [['banner_image', 'name'], 'string', 'max' => 255],
            [['banner_image_file'], 'safe'],
            [['banner_image_file'], 'file', 'extensions'=>'jpg, jpeg, png']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('banner', 'Name'),
            'banner_image' => Yii::t('banner', 'Banner Image'),
            'status' => Yii::t('banner', 'Status'),
        ];
    }

    public function getImageFile($attribute='banner_image')
    {
        return isset($this->$attribute) && !empty($this->$attribute) ? Yii::$app->params['bannerUploadPath'] . $this->$attribute : null;
    }

    public function getImageUrl($attribute='banner_image')
    {
        return isset($this->$attribute) && !empty($this->$attribute) ? Yii::$app->params['bannerUploadUrl'] . $this->$attribute : '/images/default.png';
    }

    public function uploadImage($attribute='banner_image_file') {
        $image = UploadedFile::getInstance($this, $attribute);

        // if no image was uploaded abort the upload
        if (empty($image)) {
            return false;
        }

        $ext = end((explode(".", $image->name)));

        // generate a unique file name
        $imgAttributeMap = [
                'banner_image_file' => 'banner_image',
                // 'feature_image_file_2' => 'feature_image_2',
                // 'feature_image_file_3' => 'feature_image_3',
                // 'feature_image_file_4' => 'feature_image_4',
                // 'feature_image_file_5' => 'feature_image_5'
            ];
        $this->$imgAttributeMap[$attribute] = Yii::$app->security->generateRandomString().".{$ext}";

        // the uploaded image instance
        return $image;
    }

    public function deleteImage($attribute='banner_image') {
        $file = $this->getImageFile($attribute);

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->$attribute = null;

        return true;
    }

    public function beforeDelete() {
        foreach(['banner_image'] as $attribute) {
            $this->deleteImage($attribute);
        }
        return true;
    }
}

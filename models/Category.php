<?php

namespace app\models;

use Yii;
use app\models\Product;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $promotion_id
 * @property string $name
 * @property integer $status
 */
class Category extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'status', 'num_items'], 'integer'],
            [['name', 'is_feature'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => Yii::t('category', 'Parent'),
            'name' => Yii::t('category', 'Name'),
            'status' => Yii::t('category', 'Status'),
            'is_feature' => Yii::t('category', 'Show on Homepage'),
            'num_items' => Yii::t('category', 'Number of items show on homepage')
        ];
    }

    public function getParent() {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    public function getChildren() {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    public function getProducts($order = SORT_ASC) {
        return Product::find()
            ->where(['category_id' => $this->getChildren()->select(['id'])->column()])
            ->orWhere(['category_id' => $this->id])
            ->orderBy(['id' => $order]);
    }

}

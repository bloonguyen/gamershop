<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_order".
 *
 * @property integer $id
 * @property string $customer_name
 * @property string $address
 * @property string $phone
 * @property string $note
 * @property string $payment_method
 * @property integer $status
 */
class ProductOrder extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_CANCELED = -1;

    const PAYMENT_COD = "COD";
    const PAYMENT_TRANSFER = "TRANSFER";
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_name', 'address', 'phone'], 'required'],
            [['note'], 'string'],
            [['status'], 'integer'],
            [['customer_name', 'address', 'phone', 'payment_method'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('product_order', 'ID'),
            'customer_name' => Yii::t('product_order', 'Customer Name'),
            'address' => Yii::t('product_order', 'Address'),
            'phone' => Yii::t('product_order', 'Phone'),
            'note' => Yii::t('product_order', 'Note'),
            'payment_method' => Yii::t('product_order', 'Payment Method'),
            'status' => Yii::t('product_order', 'Status'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->code = strtoupper(substr(Yii::$app->security->generateRandomString(), 0, 8));
            return true;
        } else {
            return false;
        }
    }

    public function getItems(){
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }
}

-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 19, 2016 at 05:01 AM
-- Server version: 5.5.47
-- PHP Version: 5.5.33-1+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vagrant`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('administrator', '1', 1458052175);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrator', 1, NULL, NULL, NULL, 1458052137, 1458052137),
('moderator', 1, NULL, NULL, NULL, 1458052137, 1458052137);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('administrator', 'moderator');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `name`, `banner_image`, `status`) VALUES
(14, 'Some Event', '1rM49TAXBA3HKV1vcojRl29Nfx6AFFtt.jpg', 0),
(15, 'Sale', 'xdN-LoByEOLId9kyexFB5WPBNANAzPtP.jpg', 1),
(16, 'test', 'NQGHKBnSexWpteeRj2wMrs44q2ZQHxTT.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_feature` tinyint(1) NOT NULL DEFAULT '0',
  `num_items` int(11) NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `status`, `is_feature`, `num_items`) VALUES
(4, NULL, 'Keyboard', 1, 1, 6),
(5, NULL, 'Other', 1, 1, 3),
(6, NULL, '???', 1, 1, 3),
(7, 4, 'Logitech', 1, 0, 3),
(8, 5, 'Mousepad', 1, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1458050383),
('m140506_102106_rbac_init', 1458052113),
('m151221_212811_create_table_user', 1458052120),
('m160306_042243_create_promotion_table', 1458052120),
('m160306_043311_create_category_table', 1458052120),
('m160306_044632_create_product_table', 1458052120),
('m160306_050121_create_order_table', 1458052120),
('m160306_051020_create_order_item_table', 1458052120),
('m160310_094720_drop_promotion_id_from_category_table', 1458052120),
('m160310_095057_add_category_id_to_product', 1458052120),
('m160310_104723_add_feature_image_to_product', 1458052120),
('m160311_130007_add_is_feature_column_to_category', 1458052120),
('m160314_160523_create_banner_table', 1458181442),
('m160321_124620_add_code_to_order', 1458564804);

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE IF NOT EXISTS `order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL,
  `discounted_price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`id`, `order_id`, `product_id`, `quantity`, `price`, `discounted_price`) VALUES
(1, 8, 4, 3, 150000, 150000),
(2, 8, 5, 1, 20000, 20000),
(3, 9, 3, 1, 700000, 700000),
(4, 9, 5, 5, 20000, 20000),
(5, 10, 6, 1, 33333, 33333),
(6, 10, 4, 2, 150000, 150000),
(7, 11, 3, 1, 700000, 700000),
(8, 12, 3, 3, 700000, 700000),
(9, 12, 6, 4, 33333, 33333),
(10, 17, 5, 4, 80000, 80000),
(11, 18, 2, 5, 600000, 600000),
(12, 19, 2, 5, 600000, 600000);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `promotion_id` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `category_id` int(11) DEFAULT NULL,
  `feature_image_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feature_image_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feature_image_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feature_image_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feature_image_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `promotion_id`, `price`, `description`, `status`, `category_id`, `feature_image_1`, `feature_image_2`, `feature_image_3`, `feature_image_4`, `feature_image_5`) VALUES
(2, 'XXX', 2, 120000, '<h2>eaet</h2>\r\n<p>something</p>', 1, 5, '6tThGO6i35nU1WiTLy-mfTfF_irSLixN.jpg', 'G_6RsoZSxlMnSblszb7ijg9SWxWAVf0X.jpg', 'JvBWBuaUMkhSCPWciKmYU1V28Z8qq4OW.jpg', '9UuVv0pqt5r9u1gafT28aeZgOhw3oddF.jpg', 'O5dxke3JqZy96NwgWmQiQ-tqBAk33bP6.jpg'),
(3, 'AAA', 1, 700000, 'sdds', 1, 4, 'EXaN1_4Kkpnp-irtpHBmfy8J7Bi_U8yA.jpg', 'NamqCd-feiucg6Of0xElag51FRVZPN8U.jpg', 'ZtR3MoWCQdvgQTnbKgAO0ALhtxYPveBD.jpg', 'Gm7rz_f5pUbuPeru7tjlCeEWIF5cX4Yc.jpg', 'tknic5i9DEyLM1OaGtSoUG7xDpmQ3Z9o.jpg'),
(4, 'ddd', 2, 150000, 'lol', 1, 5, 'pUkXJswah_LQ0kbT_uZSpXguBafSwoJ-.jpg', 'P-ah2F51PkN38iFgmRbxt1w-xSAAHQff.jpg', 'ys5wlCbsQNuPavy1ia1FxVgXPOZTzK-d.jpg', 'LTE72oWnoV7z9gobsfgFWfokTbmJYzDv.jpg', ''),
(5, 'asd', 1, 20000, 'jkhoi', 1, 5, 'YX494OggwXx2QxJkLe8VqKDRAEIw-DPr.jpg', 'Pjq0RQwRxRzWyMEvxJnzPM-0eeeW3qpr.jpg', 'SCSmsj5FDnzl4qnvKXZ926AvGm16jprw.jpg', '', ''),
(6, 'bcf', 1, 33333, 'gfju', 1, 5, '2AkLU60JjiWUpOz7rnpkLRkxv2Tfujre.jpg', '84oEh7CRAXifm_B-q07I68HvFFK512_I.jpg', 've6Bph47NlLcKpa6OziWOjWG_SqWYzXR.jpg', '', ''),
(7, 'something', 1, 150000, 'asafqw', 1, 5, 'cYtaXjT8us0cy_oKe5jqVAd742ww9YBE.PNG', 'JPzqdYZFzLHdhCApYyCyLDakEjbJq0m6.jpg', 'dh5lJky1K0DyccKI7x6EKTRw3cmtRysE.jpg', 'Ja7UFGf3w_xTxIWGA_x2afzrISZCA2ly.jpg', ''),
(8, 'efga', 2, 345000, 'aqwe', 1, 6, 'Qt5kPrucvY1bibBi63VZNZZ4hA-gwDW9.jpg', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE IF NOT EXISTS `product_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cod',
  `status` int(11) NOT NULL DEFAULT '1',
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `product_order`
--

INSERT INTO `product_order` (`id`, `customer_name`, `address`, `phone`, `note`, `payment_method`, `status`, `code`) VALUES
(1, 'Master', 'Hell', '66666666666', '', 'COD', 1, ''),
(2, 'Master', 'Hell', '66666666666', '', 'COD', 1, ''),
(3, 'Master', 'Hell', '66666666666', '', 'COD', 1, ''),
(4, 'asdss', 'dfdsf', '66666666666', '', 'TRANSFER', 1, ''),
(5, 'sad', 'Hell', '66666666666', '', 'TRANSFER', -1, ''),
(6, 'Master', 'Hell', '66666666666', '', 'COD', 1, ''),
(7, 'asdss', 'dfdsf', '66666666666', '', 'TRANSFER', 1, ''),
(8, 'asdss', 'dfdsf', '66666666666', '', 'TRANSFER', 1, ''),
(9, 'frdsdsf', 'dfdsf', '4454554', 'df', 'COD', 1, ''),
(10, 'Master', 'Hell', '66666666666', '', 'TRANSFER', 1, 'WLOSSNCH'),
(11, 'Master', 'Hell', '66666666666', '', 'COD', 1, 'A0NOSC_D'),
(12, 'asdss', 'Hell', '06549874', '', 'COD', 1, 'FZZZ8-4J'),
(13, 'asdss', 'Hell', '06549874', '', 'COD', 1, 'UAC3PWVK'),
(14, 'asdss', 'Hell', '06549874', '', 'COD', 1, '3Q3GA0OD'),
(15, 'asdsssdsd', 'Hell', '06549874', 'dssdsd', 'COD', 1, '38-RIZS0'),
(16, 'asdsssdsd', 'Hell', '06549874', 'dssdsd', 'COD', 1, '86JLAXVR'),
(17, 'Master', 'Hell', '66666666666', '', 'COD', 1, 'OJCC9ONW'),
(18, 'Master', 'Hell', '66666666666', '', 'COD', 1, 'OXBFFM0I'),
(19, 'adsgasgasdgasd', 'qwqwrsas', '4658963111', '', 'COD', 0, 'TIKZLUXZ');

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE IF NOT EXISTS `promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `name`, `rate`, `type`, `start_date`, `end_date`) VALUES
(1, 'assaas', 30, 0, '2016-03-29', '2016-04-20'),
(2, 'dssdsdsdsd', 70, 0, '2016-04-07', '2016-04-16');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `password_hash`, `email`, `status`, `auth_key`, `password_reset_token`, `access_token`, `created_at`, `updated_at`) VALUES
(1, 'bloo', 'Bloo', '$2y$13$OpaC0VaJU4gQcumgrof5H.uNfa3xtnD7qF0v9uqr30C7VD8paxMfW', 'bloonguyen@theitfox.com', 1, '6sOqz6G49aw8yM6cuG4At0v6TVmfJDbT', NULL, 'hdydefMQ-YGxcsOsjiOtD05P9EEg4OzY', '2016-03-15 14:29:34', '2016-03-15 14:29:34');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

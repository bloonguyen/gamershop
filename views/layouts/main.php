<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Category;

AppAsset::register($this);

$isAdmin = !Yii::$app->user->isGuest && Yii::$app->user->identity->getRole() === "administrator";
$isAuthenticated = !Yii::$app->user->isGuest;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <?php $this->head() ?>
</head>
<body class="main">
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <?php $this->beginBody() ?>
    <div class="container">
        <div class="row" id="top">
            <div class="">  
                <div class="row" id="uptop">
                    <div class="col-md-12 clearfix">
                        <p class="pull-left">Hotline: 0979-077-067</p>
                        <div class="pull-right top-right">
                            <a class="cart-link" href="/home/cart">
                                <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"><span class="badge"><?= sizeof(Yii::$app->cart->getPositions()) ?></span></span>
                            </a>
                            <?= Html::beginForm('/home/language') ?>   
                            <?= Html::dropDownList('language', Yii::$app->language, ['en_GB' => 'EN', 'vi' => 'VI'], ['onchange'=>'this.form.submit()']) ?>
                            <?= Html::endForm() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">GAMERSHOP</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" role="navigation">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?= Yii::t('main', 'Products') ?></a>
                                <ul class="dropdown-menu" role="menu">
                                    <? foreach (Category::find()->where(['parent_id' => null])->all() as $category): ?>
                                    <?
                                        $children = $category->children;
                                        $hasChildren = count($children) > 0;
                                    ?>
                                    <? if($hasChildren): ?>
                                        <li class="dropdown-submenu">
                                            <? if ($category->name !== "Other"): ?>
                                                <a href="/home/category/<?= $category->id ?>" class="dropdown-toggle" ><?= $category->name ?></a>
                                            <? endif; ?>
                                            <ul class="dropdown-menu" role="menu">
                                                <? foreach ($children as $child): ?>
                                                    <li><a href="/home/category/<?= $child->id ?>"><?= $child->name ?></a></li>
                                                <? endforeach; ?>
                                            </ul>
                                        </li>
                                    <? else: ?>
                                        <? if ($category->name !== "Other"): ?>
                                            <li><a href="/home/category/<?= $category->id ?>"><?= $category->name ?></a></li>
                                        <? endif; ?>
                                    <? endif;?>
                                    <? endforeach; ?>
                                    <? $other = Category::find()->where(["name" => "Other"])->one(); ?>
                                    <? if ($other !== NULL): ?>
                                        <li><a href="/home/category/<?= $other->id ?>"><?= $other->name ?></a></li>
                                    <? endif; ?>
                                </ul>
                            </li>
                            <li><a href="/home/contact"><?= Yii::t('main', 'Contact') ?></a></li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search" action="/home/search" method="GET">
                            <div class="input-group">
                                <input name="query" type="text" class="form-control" placeholder="<?= Yii::t('main', 'Search') ?>">
                                <span class="input-group-btn"><button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button></span>
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
        </div>
    </div>

    <div class="container">
    <?= $content ?>
    </div>

    <!-- Footer -->
    <div class="container">
    <div class="row">
        <footer class="footer">
            <div class="container-fluid">
                <div class="col-md-6">
                    <h2>Footer</h2>
                </div>
                <div class="col-md-3">
                    <div class="col-md-6">
                        <div class="panel panel-default footer-panel">
                            <div class="panel-body">
                                <span class="glyphicon glyphicon-road"></span>
                            </div>
                        </div>
                    </div>
                    <h4>National Delivery</h4>
                </div>
                <div class="col-md-3">
                    <div class="col-md-6">
                        <div class="panel panel-default footer-panel">
                            <div class="panel-body">
                                <span class="glyphicon glyphicon-open"></span>
                            </div>
                        </div>
                    </div>
                    <h4>Upgrade<br/>Support</h4>
                </div>
            </div>
        </footer>
    </div>
    </div>

    <!-- Chat -->
    <div class="row">
            <div class="col-md-4 col-md-offset-8">
                <div class="panel panel-default chat">
                    <div class="panel-heading" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        <h3 class="panel-title"><?= Yii::t('main', 'Chat with us') ?></h3>
                    </div>
                    <div class="collapse" id="collapseExample">
                        <div class="panel-body fb-page" data-href="https://www.facebook.com/Gamer-Shop-167156100332320/" data-tabs="messages" data-width="300" data-height="400" data-small-header="true" data-adapt-container-width="false" data-hide-cover="true" data-show-facepile="true">
                            <div class="fb-xfbml-parse-ignore">
                                <blockquote cite="https://www.facebook.com/Gamer-Shop-167156100332320/">
                                    <a href="https://www.facebook.com/Gamer-Shop-167156100332320/">Gamer Shop</a>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.carousel').carousel({
            interval: 3000
        })
    </script>
  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
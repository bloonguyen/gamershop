<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

AdminAsset::register($this);

$isAdmin = !Yii::$app->user->isGuest && Yii::$app->user->identity->getRole() === "administrator";
$isAuthenticated = !Yii::$app->user->isGuest;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'GAMERSHOP',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            [
                'label' => 'Management',
                'items' => [
                    [
                        'label' => 'Users',
                        'url' => ['/admin/user/index'],
                        'visible' => $isAdmin
                    ],

                    [
                        'label' => 'Categories',
                        'url' => ['/admin/category/index'],
                        'visible' => $isAdmin
                    ],

                    [
                        'label' => 'Products',
                        'url' => ['/admin/product/index'],
                        'visible' => $isAdmin
                    ],

                    [
                        'label' => 'Promotions',
                        'url' => ['/admin/promotion/index'],
                        'visible' => $isAdmin
                    ],
                    [
                        'label' => 'Orders',
                        'url' => ['/admin/order/index'],
                        'visible' => $isAdmin
                    ],
                    [
                        'label' => 'Banners',
                        'url' => ['/admin/banner/index'],
                        'visible' => $isAdmin
                    ]

                ],
                'visible' => $isAdmin
            ],
            ['label' => 'Profile', 'url' => ['/profile/index'], 'visible' => $isAuthenticated],
            $isAuthenticated ?
                [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/home/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ] :
                ['label' => 'Login', 'url' => ['/home/login']],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; TheITFox <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

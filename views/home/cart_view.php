<?
$products = Yii::$app->cart->getPositions();
$this->title = 'Gamershop | View Cart';

?>

<div class="row">
	<ul class="order-steps">
		<li><span class="active">1</span><?= Yii::t('cart', 'Verify Cart') ?></li>
		<li><span>2</span><?= Yii::t('cart', 'Provide Information & Confirm Order') ?></li>
    <li><span>3</span><?= Yii::t('cart', 'Receive Order Code') ?></li>
	</ul>
  <? if(sizeof($products) !== 0): ?>
  	<table class="table cart-table">
  		<tr>
      		<th><?= Yii::t('cart', 'Product') ?></th>
      		<th><?= Yii::t('cart', 'Description') ?></th>
      		<th><?= Yii::t('cart', 'Quantity') ?></th>
          <th><?= Yii::t('cart', 'Amount') ?></th>
          <th></th>
      </tr>
  <!--   		<tr>
      		<td><img src="/images/5.png" alt="..."/></td>
      		<td>Steelseries Stratus XL</td>
      		<td>1
      			
      			<div class="qty_box">
    					<i id="qty_down"> - </i>
    					<i id="qty_val"> 1 </i>
    					<i id="qty_up"> + </i>
    				</div>
      		</td>
      		<td>1.290.000 vnd</td>
      		<td><span class="glyphicon glyphicon-trash cart-delete-icon" aria-hidden="true"></span></td>
    		</tr> -->
        <? foreach ($products as $product): ?>
    		<tr>
      		<td><img src="<?= $product->getImageUrl('feature_image_1'); ?>" alt="..."/></td>
      		<td><?= $product->name ?></td>
      		<td><?= $product->getQuantity()?></td>
      		<td><?= number_format($product->getCost(true)) ?> VND</td>
      		<td><span data-item-id="<?= $product->id ?>" class="glyphicon glyphicon-trash cart-delete-icon btn-rm-cart" aria-hidden="true"></span></td>
    		</tr>
        <? endforeach; ?>
  	</table>

  	<div class="row">
  		<div class="col-md-6 col-md-offset-6">
  			<table class="table">
  				<tr class="total">
  	    		<th><?= Yii::t('cart', 'Total') ?>:</th>
  	    		<th><?= number_format(Yii::$app->cart->getCost(true)) ?> VND</th>
  	    		<th></th>
  	  		</tr>
  		  </table>
  		  	<div class="col-md-4 pull-right">
  			  	<a href="/home/order"><button type="button" class="btn btn-lg pull-right" id="proceed"><?= Yii::t('cart', 'Proceed') ?></button></a>
  			  </div>
  		</div>
  	</div>
  <? else: ?>
    <div class="row">
      <div class="col-md-4 col-md-offset-1">
        <h3><?= Yii::t('cart', 'Cart is empty') ?><br/><br/></h3>
      </div>
    </div>
  <? endif; ?>

</div>

<?
$js = <<<EOT
$('.btn-rm-cart').click(function() {
  itemId = $(this).data('item-id');
  var csrfToken = $('meta[name="csrf-token"]').attr("content");
  $.post('/home/remove-from-cart/' + itemId, {_csrf : csrfToken}, function(data) {
    location.reload();
  }, "json");
}); 
EOT;
$this->registerJs($js, $this::POS_READY);
?>
<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
use app\models\ProductOrder;
$this->title = 'Gamershop | Order Information';

?>

<?
$products = Yii::$app->cart->getPositions();
?>

<div class="row">
    <ul class="order-steps">
        <li><span>1</span><?= Yii::t('cart', 'Verify Cart') ?></li>
        <li><span class="active">2</span><?= Yii::t('cart', 'Provide Information & Confirm Order') ?></li>
        <li><span>3</span><?= Yii::t('cart', 'Receive Order Code') ?></li>
    </ul>
</div>

<div class="row">
  <div class="col-md-10 col-md-offset-1">
    <div class="row">
        <div class="col-md-6">
        <div class="panel panel-default order-panel">
          <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('order', 'Cart Information') ?></h3>
          </div>
          <div class="well">
            <div class="panel-body">
              <table class="table cart-info">
              <? foreach ($products as $product): ?>
                <tr>
                  <td class="pull-left"><?= $product->getQuantity()?></td>
                  <td class="pull-left"> x </td>
                  <td class="pull-left"><?= $product->name ?></td>
                  <td><?= number_format($product->getCost(true)) ?> VND</td>
                </tr>
              <? endforeach; ?>
              </table>
              <table class="table cart-info">
                        <tr class="info-total">
                          <th class="pull-left"><?= Yii::t('cart', 'Total') ?>:</th>
                          <th><?= number_format(Yii::$app->cart->getCost(true)) ?> VND</th>
                      </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="panel panel-default order-panel"> 
          <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('order', 'Customer Information') ?></h3>
          </div>
          <div class="well">
            <div class="panel-body">
              <?= $this->render('_order_form', [
                'model' => $model,
              ]) ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-md-4">
    <a href="/home/item/<?= $model->id?>" class="product-link">
        <div class="thumbnail">
        	<div class="product-item">
            	<img src="<?= $model->getImageUrl('feature_image_1') ?>" alt="..."/>
            </div>
            <div class="caption product-description">
                <h3><?= $model->name ?></h3>
                <? $hasPromotion = $model->promotion !== null ?>
                <p style="<?= $hasPromotion ? "text-decoration: line-through; color: #666;" : ""?>"><?= number_format($model-> price) ?> VND</p>
                <? if ($hasPromotion): ?>
                    <p class="discounted-price" style="color: #169A16; font-weight: bolder;"><?= number_format($model-> getPrice(true)) ?> VND</p>
                <? endif; ?>
            </div>
        </div>
    </a>
</div>
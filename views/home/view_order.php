<?
$items = $order->items;
$total = 0;
?>

<h2><span style="color:#2F752F; font-weight:bold;"><?= Yii::t('order', 'Order: {order_code}', ['order_code' => $order->code]) ?></span></h2><br/>

<table class="table cart-table">
	<tr>
  		<th><?= Yii::t('order', 'Product Name') ?></th>
  		<th><?= Yii::t('order', 'Quantity') ?></th>
      <th><?= Yii::t('order', 'Price') ?></th>
      <th><?= Yii::t('order', 'Discounted Price') ?></th>
  </tr>

    <? foreach ($items as $item): ?>
  		<tr>
    		<td><?= $item->product->name ?></td>
    		<td><?= $item->quantity?></td>
    		<td><?= number_format($item->price) ?> VND</td>
        <td><?= number_format($item->discounted_price) ?> VND</td>
  		</tr>
      <? $total+=$item->discounted_price ?>
    <? endforeach; ?>
</table>

<div class="row">
	<div class="col-md-6 col-md-offset-6">
		<table class="table">
			<tr class="total">
    		<th><?= Yii::t('cart', 'Total') ?>:</th>
    		<th><?= number_format($total) ?> VND</th>
    		<th></th>
  		</tr>
	  </table>
</div>
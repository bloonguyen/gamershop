<?
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use app\models\Category;

$this->title = 'Gamershop | Product List';

?>

<div class="row">
    <div class="panel panel-default product-panel">
        <? //foreach (Category::find()->where(['parent_id' => null])->all() as $category): ?>
    
        <div class="panel-heading">
            <? if (isset($category) && $category != null): ?>
            <a href="#" class="panel-title"><?= $category->name ?></a>
            <? else: ?>
            <span class="panel-title">Results:</span>
            <? endif ?>

        </div>
        <div class="panel-body">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_product_view',
                'layout' =>
<<<EOT
<div class="row">
{items}
</div>
<div class="product-pager clearfix" style="text-align: center;">
{pager}
</div>
EOT
                ]); ?>
        </div>
    </div>
</div>
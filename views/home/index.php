<?
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use app\models\Category;
use app\models\Banner;

$this->title = 'Gamershop | Home';
?>

<? function renderImage($model, $index) { ?>
    <? if ($model->status = Banner::STATUS_ACTIVE): ?>
        <div class="item <?= $index==0 ? "active" : "" ?>" > 
            <img src="<?= $model->getImageUrl(); ?>" alt="<?= $model->name ?>"/>
        </div>
    <? endif; ?>
<? } ?>

<? function renderImageIndicators($model, $index) { ?>
    <? if ($model->status = Banner::STATUS_ACTIVE): ?> 
    <li data-target="#main-carousel" data-slide-to="<?= $index ?>" class="<?= $index == 0 ? "active" : "" ?>"></li>
    <? endif; ?>
<? } 
?>
<!-- Carousel -->
<div class="row">
    <div class="bs-example">
        <div id="main-carousel" class="carousel slide" data-ride="carousel">
            <? $banners = Banner::find()->where(['status' => Banner::STATUS_ACTIVE])->all(); ?>

            <!-- Carousel indicators -->
            <ol class="carousel-indicators">
                <? for ($i=0; $i < count($banners); $i++) { 
                    $banner = $banners[$i];
                    echo renderImageIndicators($banner, $i);
                }?>
            </ol>   
            <!-- Wrapper for carousel items -->
            <div class="carousel-inner">
                <? for ($i=0; $i < count($banners); $i++) { 
                    $banner = $banners[$i];
                    echo renderImage($banner, $i);
                }?>
            </div>
            <!-- Carousel controls -->
            <a class="carousel-control left" href="#main-carousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="carousel-control right" href="#main-carousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
</div>

<!-- Products -->
<div class="row">
    <div class="panel panel-default index-panel">
        <? foreach (Category::find()->where(['parent_id' => null])->all() as $category): ?>
        <? if ($category->name !== "Other"): ?>
        <?
            $dataProvider = new ActiveDataProvider([
                'query' => $category->getProducts(SORT_DESC),
                'pagination' => [
                    'pageSize' => $category->num_items
                ]
            ]);
        ?>
        <div class="panel-heading">
            <a href="/home/category/<?= $category->id ?>" class="panel-title"><?= $category->name ?></a>
        </div>
        <div class="panel-body">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_product_home_view',
                'layout' =>
<<<EOT
<div class="row">
{items}
</div>
<div class="product-pager clearfix">
{pager}
</div>
EOT
                ]); ?>
        </div>
        <? endif; ?>
        <? endforeach; ?>
        <? $other = Category::find()->where(["name" => "Other"])->one(); ?>
        <?
            $dataProvider = new ActiveDataProvider([
                'query' => $other->getProducts(SORT_DESC),
                'pagination' => [
                    'pageSize' => $other->num_items
                ]
            ]);
        ?>
        <div class="panel-heading">
            <a href="/home/category/<?= $other->id ?>" class="panel-title"><?= $other->name ?></a>
        </div>
        <div class="panel-body">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_product_home_view',
                'layout' =>
<<<EOT
<div class="row">
{items}
</div>
<div class="product-pager clearfix">
{pager}
</div>
EOT
                ]); ?>
        </div>
    </div>
</div>
<? $this->title = 'Gamershop | Contact'; ?>
<h1><?= Yii::t('contact', 'Our Bank Accounts Information') ?></h1>
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default bank">
			<div class="panel-body">
				<img src="/images/ACB.png" style="width:250px; height:auto"/>
				<p class="bank-detail"><?= Yii::t('contact', 'Account Owner: ') ?>Trần Kim Hoàng </p>
				<p class="bank-detail"><?= Yii::t('contact', 'Account Number: ') ?>113670249</p>
				<p class="bank-detail"><?= Yii::t('contact', 'Bank Address: ') ?>ACB chi nhánh Minh Châu TPHCM</p>
			</div>
		</div>
	</div>
</div>
<p><?= Yii::t('contact', 'If you have any question, please contact us via our ') ?> <span class="text-danger"><?= Yii::t('contact', 'hotline: ') ?>0979 077 067</span></p>

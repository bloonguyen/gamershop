<?php

use yii\helpers\Html;
use app\models\Product;
$this->title = 'Gamershop | Product Detail';

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>
<? function renderImage($model, $index, $attribute) { ?>
	<? if (!empty($model->$attribute)): ?> 
		<div class="item <?= $index==0 ? "active" : "" ?>" > 
			<img src="<?= $model->getImageUrl($attribute); ?>" alt="<?= $model->name ?>"/>
		</div>
	<? endif; ?>
<? } ?>

<? function renderImageThumbnail($model, $index, $attribute) { ?>
	<? if (!empty($model->$attribute)): ?> 
	<li data-target="#product-carousel" data-slide-to="<?= $index ?>" class="<?= $index == 0 ? "active" : "" ?>">
		<div class="thumbnail"> 
			<img src="<?= $model->getImageUrl($attribute); ?>" alt="<?= $model->name ?>"/>
		</div>
	</li>
	<? endif; ?>
<? } 
?>

<div class="row">
	<div class="row">
		<div class="col-md-6">
			<div class="bs-example">
    			<div id="product-carousel" class="carousel slide item-carousel" data-ride="carousel">
		        	<!-- Wrapper for carousel items -->
		        	<div class="carousel-inner">
		        		<? for ($i=1; $i < 6; $i++) { 
		        			$attribute = 'feature_image_' . $i;
		        			echo renderImage($model, $i-1, $attribute);
		        		}?>
		        	</div>
		        	<!-- Carousel indicators -->
		        	<ol class="carousel-indicators">
		        		<? for ($i=1; $i < 6; $i++) { 
		        			$attribute = 'feature_image_' . $i;
		        			echo renderImageThumbnail($model, $i-1, $attribute);
		        		}?>
		        	</ol>   
		        </div>
			</div>
		</div>
		<div class="col-md-6">
			<div id="item-panel" class="panel panel-default">
				<div class="panel-heading">
					<h2 class="panel-title"><?= $model->name ?></h2>
					</div>
					<div class="panel-body">
						<? $hasPromotion = $model->promotion !== null ?> 
						<div class="price"><h2 style="<?= $hasPromotion ? "text-decoration: line-through; color: #666;" : ""?>"><?= number_format($model->price) ?> VND</h2>
							<? if ($hasPromotion): ?>
	                    		<h2 class="discounted-price" style="color: #169A16; font-weight: bolder;"><?= number_format($model-> getPrice(true)) ?> VND</h2>
	                		<? endif; ?>
                		</div>
						<div class="p_info">
							<div class="p_detail">
								<?= $model->description ?>
							</div>
							<div class="qty_box">
								<i id="qty_down"> - </i>
								<i id="qty_val"> 1 </i>
								<i id="qty_up"> + </i>
							</div>
							<div class="add">
								<button type="button" data-item-id="<?= $model->id ?>" class="btn btn-danger btn-lg btn-cart">Add to cart</button>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>

<?
$js = <<<EOT
$('i#qty_down').click(function(){
	var val = parseInt($('i#qty_val').html(), 10);
	if(isNaN(val)) {
		val = 1;
	}
	val--;
	if(val <= 0) {
		val = 1;
	}
	$('i#qty_val').html(val);
});
$('i#qty_up').click(function(){
	var val = parseInt($('i#qty_val').html(), 10);
	if(isNaN(val)) {
		val = 1;
	}
	val++;
	$('i#qty_val').html(val);
});
$('.btn-cart').click(function() {
	itemId = $(this).data('item-id');
	var csrfToken = $('meta[name="csrf-token"]').attr("content");
	$.post('/home/add-to-cart/' + itemId, {_csrf : csrfToken, quantity : $('i#qty_val').html()}, function(data) {
		location.reload();
	}, "json");
}); 
EOT;
$this->registerJs($js, $this::POS_READY);
?>
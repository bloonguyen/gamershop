<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
use app\models\ProductOrder;
$this->title = 'Gamershop | Confirm Order';

?>

<div class="row">
	<ul class="order-steps">
		<li><span>1</span><?= Yii::t('cart', 'Verify Cart') ?></li>
		<li><span>2</span><?= Yii::t('cart', 'Provide Information & Confirm Order') ?></li>
    <li><span class="active">3</span><?= Yii::t('cart', 'Receive Order Code') ?></li>
	</ul>
</div>

<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <div class="panel panel-success order-success">
      <div class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('order', 'Order successfully recorded') ?></h3>
      </div>
      <div class="panel-body">
        <span style="font-size: 20px;"><?= Yii::t('order', 'Order Code') ?>: <span style="font-weight: bold;"><?= $code ?></span></span><br/><br/>
        <?= Yii::t('order', 'Enter this code to the search bar to track your order.') ?>
      </div>
    </div>
  </div>
</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\ProductOrder;

/* @var $this yii\web\View */
/* @var $model app\models\ProductOrder */
/* @var $form yii\widgets\ActiveForm */
$model->status = ProductOrder::STATUS_ACTIVE;
?>

<div class="product-order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'payment_method')->dropDownList([ProductOrder::PAYMENT_COD => 'COD', ProductOrder::PAYMENT_TRANSFER => 'TRANSFER']) ?>

    <div class="form-group pull-right">
        <?= Html::submitButton(Yii::t('order', 'Confirm'), ['class' => 'btn btn-lg proceed']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

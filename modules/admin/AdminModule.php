<?php

namespace app\modules\admin;

class AdminModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\admin\controllers';

    public function init()
    {
        parent::init();
        $this->layoutPath = \Yii::getAlias('@app/views/layouts/');
        $this->layout = 'admin';
    }
}

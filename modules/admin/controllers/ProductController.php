<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Product;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                    [
                        'allow' => false,
                        'verbs' => ['GET'],
                        'actions' => ['delete', 'delete-timer']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post())) {
            $imgAttributes = ['feature_image_1', 'feature_image_2', 'feature_image_3', 'feature_image_4', 'feature_image_5'];
            $fields = ['feature_image_file_1', 'feature_image_file_2', 'feature_image_file_3', 'feature_image_file_4', 'feature_image_file_5'];
            $images = [];
            foreach($fields as $field) {
                $image = $model->uploadImage($field);
                array_push($images, $image);
            }

            if ($model->save()) {
                // upload only if valid uploaded file instance found
                for($i = 0; $i < count($images); $i++) {
                    $image = $images[$i];
                    if ($image !== false) {
                        $path = $model->getImageFile($imgAttributes[$i]);
                        if($path) {
                            $image->saveAs($path);
                        }
                    }
                }
                return $this->redirect(['view', 'id'=>$model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $imgAttributes = ['feature_image_1', 'feature_image_2', 'feature_image_3', 'feature_image_4', 'feature_image_5'];
        $fields = ['feature_image_file_1', 'feature_image_file_2', 'feature_image_file_3', 'feature_image_file_4', 'feature_image_file_5'];
        $oldFiles = [];
        $oldImages = [];

        for($i = 0; $i < count($fields); $i++) {
            $field = $fields[$i];
            $image = $model->getImageFile($imgAttributes[$i]);
            array_push($oldFiles, $image);
            array_push($oldImages, $model->$imgAttributes[$i]);
        }

        if ($model->load(Yii::$app->request->post())) {
            // process uploaded image file instance
            $images = [];
            foreach($fields as $field) {
                $image = $model->uploadImage($field);
                array_push($images, $image);
            }


            // revert back if no valid file instance uploaded
            for($i = 0; $i < count($images); $i++) {
                $image = $images[$i];
                if ($image === false) {
                    $model->$imgAttributes[$i] = $oldImages[$i];
                }
            }

            if ($model->save()) {
                // upload only if valid uploaded file instance found
                for($i = 0; $i < count($images); $i++) {
                    $image = $images[$i];
                    if ($image !== false) {
                        if(file_exists($oldFiles[$i])) {
                            unlink($oldFiles[$i]);
                        }
                        $path = $model->getImageFile($imgAttributes[$i]);
                        if($path) {
                            $image->saveAs($path);
                        }
                    }
                }
                return $this->redirect(['view', 'id'=>$model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    // public function actionAddToCart($id)
    // {
    //     if (($model = Product::findOne($id)) !== null) {
    //         return $cart = Yii::$app->cart->put($cartPosition, 1);
    //     } else {
    //     throw new NotFoundHttpException();
    //     }
    // }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

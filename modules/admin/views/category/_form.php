<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
$parents = ["" => ""];
foreach (ArrayHelper::map(Category::find()->where(['parent_id' => null])->all(), 'id', 'name') as $key => $value) {
    $parents[$key] = $value;
}
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->dropDownList($parents) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([Category::STATUS_ACTIVE => 'ACTIVE', Category::STATUS_INACTIVE => 'INACTIVE']) ?>

    <?= $form->field($model, 'is_feature')->checkbox()?>

	<?= $form->field($model, 'num_items')->label(Yii::t('category', "Number of lines shown on home page"))->dropDownList([3=>1,6=>2,9=>3,12=>4,15=>5,18=>6,21=>7]) ?>    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

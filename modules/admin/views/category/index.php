<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('category', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th><?= Yii::t('category', 'Name') ?></th>
                <th><?= Yii::t('category', 'Status') ?></th>
                <th style="min-width: 120px;"></th>
            </tr>
        </thead>
        <tbody>
            <? $counter = 1 ?>
            <? foreach ($categories as $index => $category): ?>
                <tr>
                    <td><?= $counter ?></td>
                    <td><?= $category->name ?></td>
                    <td><?= $category->status == Category::STATUS_ACTIVE ? 'ACTIVE' : 'INACTIVE' ?></td>
                    <td class="text-center">
                        <div class="btn-group">
                            <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['category/view', 'id' => $category->id], [
                                    'class' => 'btn btn-sm btn-primary',
                                    'data' => [
                                    ]
                                ]); ?>
                            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['category/update', 'id' => $category->id], [
                                    'class' => 'btn btn-sm btn-success',
                                    'data' => [
                                    ]
                                ]); ?>
                            <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['category/delete', 'id' => $category->id], [
                                    'class' => 'btn btn-sm btn-danger',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => 'Do you want to delete this item?'
                                    ]
                                ]); ?>
                        </div>
                    </td>
                </tr>
                <?
                    $counter++;
                    $children = $category->children;
                ?>
                <? foreach ($children as $child_index => $child): ?>
                    <tr>
                        <td><?= $counter ?></td>
                        <td> ---- <?= $child->name ?></td>
                        <td><?= $child->status == Category::STATUS_ACTIVE ? 'ACTIVE' : 'INACTIVE' ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['category/view', 'id' => $child->id], [
                                        'class' => 'btn btn-sm btn-primary',
                                        'data' => [
                                        ]
                                    ]); ?>
                                <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['category/update', 'id' => $child->id], [
                                        'class' => 'btn btn-sm btn-success',
                                        'data' => [
                                        ]
                                    ]); ?>
                                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['category/delete', 'id' => $child->id], [
                                        'class' => 'btn btn-sm btn-danger',
                                        'data' => [
                                            'method' => 'post',
                                            'confirm' => 'Do you really want to delete this item?'
                                        ]
                                ]); ?>
                            </div>
                        </td>
                    </tr>
                    <?
                        $counter++;
                    ?>
                <? endforeach; ?>
            <? endforeach; ?>
        </tbody>
    </table>

</div>

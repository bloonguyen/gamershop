<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Banner;

/* @var $this yii\web\View */
/* @var $model app\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'banner_image_file')->fileInput(['maxlength' => true]) ?>
    <?= Html::img($model->getImageUrl('banner_image'), ['height' => 100]); ?>

    <?= $form->field($model, 'status')->dropDownList([Banner::STATUS_ACTIVE => 'ACTIVE', Banner::STATUS_INACTIVE => 'INACTIVE']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

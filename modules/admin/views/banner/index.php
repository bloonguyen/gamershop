<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Banner', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'banner_image',
            'status',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="text-center"><div class="btn-group" style="min-width: 120px;">{view}{update}{delete}</div></div>',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"</span>', $url, [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'View',
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'class' => 'btn btn-success btn-sm',
                            'title' => 'Update',
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'class' => 'btn btn-danger btn-sm',
                            'data' => [
                                'method' => 'post',
                                'confirm' => 'Do you want to delete this item?'
                            ]
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>

</div>

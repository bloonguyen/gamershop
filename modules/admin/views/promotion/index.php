<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('promotion', 'Promotions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promotion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Promotion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'rate',
            'type',
            'start_date',
            // 'end_date',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="text-center"><div class="btn-group">{view} {update} {delete}</div></div>',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'class' => 'btn btn-sm btn-primary',
                                    'data' => [
                                    ]
                                ]); 
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'class' => 'btn btn-sm btn-success',
                            'data' => [
                            ]
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'class' => 'btn btn-sm btn-danger',
                            'data' => [
                                'method' => 'post',
                                'confirm' => 'Do you really want to delete this item?'
                            ]
                        ]);
                    }
                ],
            ],        
        ],
    ]); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Category;
use app\models\Product;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Promotion */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Promotions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$productProvider = new ActiveDataProvider([
    'query' => Product::find()->where(['promotion_id' => $model->id]),
]);
?>
<div class="promotion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'rate',
            'type',
            'start_date',
            'end_date',
        ],
    ]) ?>

    <h2>Applied Products:</h2>
    <?= GridView::widget([
        'dataProvider' => $productProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
        ],
    ]); ?>

</div>

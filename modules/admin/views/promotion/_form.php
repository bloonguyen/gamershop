<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
// use dosamigos\datepicker\DatePicker;
use yii\jui\DatePicker;
use app\models\Category;
use app\models\Product;
use app\models\Promotion;

/* @var $this yii\web\View */
/* @var $model app\models\Promotion */
/* @var $form yii\widgets\ActiveForm */
if (!$model->isNewRecord) {
    $model->product_ids = Product::find()->where(['promotion_id' => $model->id])->select(['id'])->column();
}

$products = [];
foreach (Category::find()->all() as $category) {
    $products[$category->name] = ArrayHelper::map(Product::find()->where(['category_id' => $category->id])->all(), 'id', 'name');
}
if (!array_key_exists("Others", $products)) {
    $products["Others"] = [];
}
$products["Others"] = array_merge($products["Others"], ArrayHelper::map(Product::find()->where(['category_id' => null])->all(), 'id', 'name'));
?>

<div class="promotion-form" style="width: 80%;margin:auto;">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rate')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([Promotion::CUSTOM => 'CUSTOM', Promotion::ALL => 'ALL']) ?>

    <div class="row">
        <div class="col-md-5">

            <?= $form->field($model, 'start_date')->widget(\yii\jui\DatePicker::classname(), [
                'dateFormat' => 'yyyy-MM-dd',
            ]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'end_date')->widget(\yii\jui\DatePicker::classname(), [
                'dateFormat' => 'yyyy-MM-dd',
            ]) ?>
        </div>
    </div>

    <div class="row">    
        <div class="col-md-6">
            <?= $form->field($model, 'product_ids')->label("Products")->dropDownList($products, ['multiple' => 'multiple']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$js = <<<EOT
$('#promotion-product_ids').multiSelect({ selectableOptgroup: true });
EOT;
$this->registerJs($js, $this::POS_READY);
?>
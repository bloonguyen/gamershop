<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\OrderItem;
/* @var $this yii\web\View */
/* @var $model app\models\ProductOrder */

$this->title = $model->customer_name;
$this->params['breadcrumbs'][] = ['label' => 'Product Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$itemProvider = new ActiveDataProvider([
    'query' => OrderItem::find()->where(['order_id' => $model->id]),
]);
?>

<div class="product-order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Archive', ['archive', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to archive this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            'customer_name',
            'address',
            'phone',
            'note:ntext',
            'payment_method',
            'status',
        ],
    ]) ?>

    <h2>Order Items:</h2>
    <?= GridView::widget([
        'dataProvider' => $itemProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'product.name',
            // [
            //     'attribute' => 'product.name',
            //     'label' => 'Product Name',
            //     // 'value'=> function ($model, $key, $index, $column) {
            //     //     return $model->product->name;
            //     // } 
            // ],
            'quantity',
            'price',
            'discounted_price'
        ],
    ]); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('order', 'Product Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::checkbox('show_archived', Yii::$app->request->get('showArchived', false), ['label' => 'Show archived?']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'customer_name',
            'address',
            'phone',
            'note:ntext',
            // 'payment_method',
            // 'status',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="text-center"><div class="btn-group" style="min-width: 102px;">{view}{update}{archive}</div></div>',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"</span>', $url, [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'View',
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'class' => 'btn btn-success btn-sm',
                            'title' => 'Update',
                        ]);
                    },
                    'archive' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-floppy-save"></span>', $url, [
                            'class' => 'btn btn-danger btn-sm',
                            'data-method' => 'post',
                            'title' => 'Archive',
                            'data-confirm' => 'Do you want to archive this item?'
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>

</div>
<?
$js = <<<EOT
$('input[name=show_archived]').change(function() {
    if($(this).is(':checked')) {
        location.href = '/admin/order/index?showArchived=1';
    } else {
        location.href = '/admin/order/index?showArchived=0';
    }
});
EOT;
$this->registerJs($js, $this::POS_READY);
?>
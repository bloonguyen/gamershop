<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Category;
use app\models\Product;
use app\models\Promotion;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('product', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'category_id',
                'value' => function ($model, $key, $index, $column) {
                    return $model->category->name;
                }
            ],
            'name',
            [
                'attribute'=>'promotion_id',
                'format' => 'html',
                'value' => function ($model, $key, $index, $column) {
                    $promotion = $model->promotion;
                    if ($promotion !== NULL) {
                        return Html::a($promotion->name, ['/admin/promotion/view', 'id' => $promotion->id]);
                    }
                    return '<span class="text-danger">(not set)</span>';
                }

            ],
            'price',
            [
                'attribute'=>'status',
                'value' => function ($model, $key, $index, $column) {
                    return $model->status == Product::STATUS_ACTIVE ? 'ACTIVE' : 'INACTIVE'; 
                }

            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="text-center"><div class="btn-group">{view} {update} {delete}</div></div>',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'class' => 'btn btn-sm btn-primary',
                                    'data' => [
                                    ]
                                ]); 
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'class' => 'btn btn-sm btn-success',
                            'data' => [
                            ]
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'class' => 'btn btn-sm btn-danger',
                            'data' => [
                                'method' => 'post',
                                'confirm' => 'Do you really want to delete this item?'
                            ]
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'promotion_id',
            'price',
            'description:html',
            [
                'attribute' => 'feature_image_1',
                'format' => 'html',
                'value' => Html::img($model->getImageUrl('feature_image_1'), ['width' => 200])
            ],
            [
                'attribute' => 'feature_image_2',
                'format' => 'html',
                'value' => Html::img($model->getImageUrl('feature_image_2'), ['width' => 200])
            ],
            [
                'attribute' => 'feature_image_3',
                'format' => 'html',
                'value' => Html::img($model->getImageUrl('feature_image_3'), ['width' => 200])
            ],
            [
                'attribute' => 'feature_image_4',
                'format' => 'html',
                'value' => Html::img($model->getImageUrl('feature_image_4'), ['width' => 200])
            ],
            [
                'attribute' => 'feature_image_5',
                'format' => 'html',
                'value' => Html::img($model->getImageUrl('feature_image_5'), ['width' => 200])
            ],
            'status',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use app\models\Product;
use app\models\Category;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
$categories = [];

foreach (Category::find()->where(["not", ["name" => "Other"]])->all() as $category) {
    $categories[$category->id] = $category->name;
}

$other = Category::find()->where(["name" => "Other"])->one();
$categories[$other->id] = $other->name;
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'language' => Yii::$app->language,
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <?//= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'category_id')->dropDownList($categories) ?>

    <?= $form->field($model, 'status')->dropDownList([Product::STATUS_ACTIVE => 'ACTIVE', Product::STATUS_INACTIVE => 'INACTIVE']) ?>

    <?= $form->field($model, 'feature_image_file_1')->fileInput(); ?>
    <?= Html::img($model->getImageUrl('feature_image_1'), ['height' => 100]); ?>
    <?= $form->field($model, 'feature_image_file_2')->fileInput(); ?>
    <?= Html::img($model->getImageUrl('feature_image_2'), ['height' => 100]); ?>
    <?= $form->field($model, 'feature_image_file_3')->fileInput(); ?>
    <?= Html::img($model->getImageUrl('feature_image_3'), ['height' => 100]); ?>
    <?= $form->field($model, 'feature_image_file_4')->fileInput(); ?>
    <?= Html::img($model->getImageUrl('feature_image_4'), ['height' => 100]); ?>
    <?= $form->field($model, 'feature_image_file_5')->fileInput(); ?>
    <?= Html::img($model->getImageUrl('feature_image_5'), ['height' => 100]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

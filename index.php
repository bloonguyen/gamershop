<?php
error_reporting(E_ALL);
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/vendor/autoload.php');
require(__DIR__ . '/vendor/yiisoft/yii2/Yii.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/config/main.php'),
    require(__DIR__ . '/config/main-local.php')
);

class YiiApplication extends yii\web\Application {

    function __construct($appConfig) {
    	parent::__construct($appConfig);
    }

    protected $deferredStack = [];

    public function __destruct() {
        $this->runDeferredStack();
    }

    public function defer(callable $deferred) {
        $this->deferredStack[] = $deferred;
    }

    protected function runDeferredStack()
    {
        array_map('call_user_func', array_reverse($this->deferredStack));
    }
}

(new YiiApplication($config))->run();

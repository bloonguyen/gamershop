<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/stylesheets/general.css',
        '/stylesheets/index.css',
        '/stylesheets/cart_view.css',
        '/stylesheets/order_info.css',
        '/stylesheets/product_detail.css',
        '/stylesheets/product_list.css',
        '/js/jquery-multiselect/jquery.multiselect.css'
    ];
    public $js = [
        '/js/jquery-1.11.3.min.js',
        '/js/jquery-ui/1.11.4/jquery-ui.min.js',
        '/js/jquery-multiselect/jquery.multiselect.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}
